// // 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// // Обработчик события - это функция, которая обрабатывает, или откликается на событие. 

// const Price = document.getElementById("price");
// const priceShow = document.getElementById("priceShow");
// const incorrektValue = document.getElementById("incorrektValue")
// priceShow.textContent = "";
// incorrektValue.textContent = "";

// Price.addEventListener('blur', (event) => {
//   if (isNaN(Price.value) || Price.value < 0 || !Price.value){
//     incorrektValue.textContent = `Please enter correct price`;
//     Price.classList.add("invalid")
//     Price.addEventListener("focus",(e) => {
//       incorrektValue.remove();
//       Price.classList.remove("invalid");
//     })
//       }else{
//   priceShow.innerHTML = `Текущая цена:${Price.value}<button>X</button>`;
//   let btn = document.querySelector("button");
//   btn.addEventListener("click", onBtnClick);
//   function onBtnClick(){
//     priceShow.remove();
//     Price.value = "";
//   }
//   }
// });



const Price = document.getElementById("price");
const priceShow = document.getElementById("priceShow");
const incorrektValue = document.getElementById("incorrektValue")

Price.addEventListener('blur', (e) => {
  if (Price.value < 0 || isNaN(+Price.value)){
    incorrektValue.textContent = `Please enter correct price`;
    Price.classList.add("invalid");
     Price.addEventListener("focus",(e) => {
       incorrektValue.textContent = "";
       Price.classList.remove("invalid");
       Price.value = "";
    })
      }
    if (Price.value > 0 && !isNaN(Price.value)){
  priceShow.innerHTML = `Текущая цена:${Price.value}<button>X</button>`;
  priceShow.addEventListener("click", onBtnClick);
  function onBtnClick(){
    priceShow.textContent = "";
    Price.value = "";
  }
  }
});