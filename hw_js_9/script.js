// let listTabs = document.querySelectorAll(".tabs-title");
// let tabsContent = document.querySelectorAll(".tab-content");

// listTabs.forEach(test) 
// function test(elem, index){
//     elem.addEventListener("click",ClikTab);
//     function ClikTab(){
//         listTabs.forEach(elem => elem.className = "tabs-title");
//         listTabs[index].className = "tabs-title active";
//         tabsContent.forEach(elem => elem.className = "tab-content");
//         tabsContent[index].className = "tab-content show";
//         }
//     }

let listTabs = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tab-content");
listTabs.forEach((item) =>{
    item.addEventListener("click", () =>{
        let currentBtn = item;
        let tabId = currentBtn.getAttribute("data-tab");
        let currentTab = document.querySelector(tabId);
        if(!currentBtn.classList.contains("active")){
            listTabs.forEach((item) => item.classList.remove("active"))
        };
        tabsContent.forEach((item) =>{
        item.classList.remove("show")
        })
        currentBtn.classList.add("active")
        currentTab.classList.add("show")
    })
})