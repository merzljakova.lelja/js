let bodyMode = document.body;
bodyMode.className = localStorage.getItem("mode");
const switcher = document.querySelector("button");
switcher.addEventListener("click", chengeTheme);
function chengeTheme() {
    
    let data = localStorage.getItem("mode");
    
    if (data === "light") {
        localStorage.setItem("mode","dark");
        bodyMode.className = "dark";
    } else {
        localStorage.setItem("mode","light");
        bodyMode.className = "light";
    }
}
